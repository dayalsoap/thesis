* Introduction
     * In situ - trend
     * Node Heterogeneity - trend
     * Bridging the gap
     * Thesis Statement
     * Contributions
     * Research Overview
* Motivation and Foundations
     * Canonical Terms?
     * Motivating Applications
          * LAMMPS
          * GTC-P
     * Foundation Technologies
          * EVPath
          * ADIOS
* Enabling in situ/in transit code coupling
     * Flexpath Paper
* Management abstractions for dynamic workflows
     * SODA paper + Transactions worked in
* CoApps runtime
     * Intro
     * CoApps abstraction
          * Location Flexibility
          * Policy specification
     * Matching function
     * Design and Implementation
         * MPI Relay
         * Interference avoidance/strategy
       * Results
* Conclusions

* Future Directions