\chapter{Introduction}
\label{intro}
\section{Trend: In Situ Analytics}
The traditional bulk synchronous model of conducting science involves simulations periodically outputting raw data to a shared storage system. At some point after the simulation has completed, the scientist then tries to make sense of the data by reading it from disk, analyzing it, and then outputting some derivation of the original data back to disk. With this approach, the reliance on disk as the coupling mechanism between simulations and analysis codes has become an impeding bottleneck. In fact, on current generation petascale machines, it is common to see I/O operations consume 50\% of the simulation’s execution time~\cite{bennett,predata}. As the compute performance is expected to outpace the disk performance, the problem is projected to get worse as we move towards exascale.

In situ analysis breaks away from this traditional model and moves to a more decoupled and asynchronous environment where analysis codes execute simultaneously with the simulation by ingesting and operating on data as it is produced. This approach enables a wealth of new functionality, such as ascertaining the simulation's validity during execution \cite{UQ}, enabling online visualization \cite{insituvis, paraview}, and even steering the simulation as it is running \cite{vettersteering}. Additionally, there are performance benefits to this approach as data does not have to go through slow storage to be analyzed. Instead, data can move directly to the analysis codes via shared memory or high-bandwidth RDMA networks typically found on modern supercomputers. Entire in situ workflows can be constructed by chaining together in situ analysis codes using a number of different code coupling mechanisms \cite{dataspaces, flexpath, flexio}.

While this approach has shown great promise, there are still challenges science end-users face in regards to finding the optimal placement of in situ analytics. In situ analytics can run in a number of locations, such as sharing cores with the simulation, sharing nodes with the simulation but running on dedicated cores, or running in the same supercomputer but using a dedicated set of ``staging'' nodes \cite{strawman}, each of which has performance and resource usage implications. This thesis wants to explore a system that can dynamically choose between these placement options at runtime to provide better guarantees for end-user goals. 


\section{Trend: Node Complexity}
Fig. \ref{f:argo-node} depicts an example of an exascale node as projected by a U.S. Department of Energy laboratory \cite{argo}. The specific software and hardware products are not the intended focus of this image, but instead, attention should be focused on the robust number of software and hardware components that science end users may have to interact with. Such a diverse environment presents science end users with a number of opportunities to accelerate their science applications, however users are required to port their applications to use new runtimes and  users must extensively profile their applications to understand which resource arrangements will yield the best performance gains for their applications.

\begin{figure*}[htbp]
\centering
\includegraphics[width=.98\textwidth]{figures/argo-node}
\caption{Projected Node Architecture for an Exascale Machine}
\vspace{-.2in}
\label {f:argo-node}
\end{figure*}

\section{Challenges with New Trends}
These new developments introduce several challenges. With in situ analytics, runtime dynamics can occur causing initial resource allocations to be sub-optimal, especially as in situ workflows grow in complexity. Dynamism can occur for several reasons, such as poor resource allocations inducing performance bottlenecks, workflow components may fail, some workflow components may become relevant due to certain features of the data being analyzed, etc. Dealing with such dynamism will require both continual monitoring of workflow components and reallocation of resources at runtime.

In order to make resource allocation decisions at runtime, we need ways to extract relevant information from the hardware and the runtimes that manage them. Additionally, we need abstractions to properly interact with and control the runtimes so we can have greater guarantees that end-user goals can be met. The goal of this thesis is to bridge the gap between the in situ analysis model of conducting science and growing node complexity by designing a system that can introspect and delegate control to runtimes so we can sustain performance and proper execution of in situ workflows.  
