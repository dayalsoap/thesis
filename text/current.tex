\chapter{Current Research and Proposed Implementation}
\label{current}
My current work in this space has looked at developing a communication mechanism for in situ workflows and developing mechanisms that allow in situ workflows to be managed at runtime. The communication mechanism uses a publish subscribe model adapted for science workloads to allow simulations and analysis codes to exchange fine grained slices of array data. Using a management hierarchy, science end users can create a variety of policies such as performance policies, data aware policies, and resilience policies, by programmatically specifying control actions to take when certain conditions are detected. The work for this thesis will be built as an extension to this existing workflow system. 

\section{Flexpath}
Flexpath \cite{flexpath} is a type-based publish/subscribe infrastructure for coupling high-end scientific applications with their in situ analytics services. Flexpath’s pub/sub approach makes possible runtime configurability, scalability, and also fault tolerance, as the pub/sub abstraction allows for the decoupling of diverse analytics components, permits multiple subscribers or publishers to share a single data stream, and suppresses communications for cases in which there are no subscribers to certain data streams (e.g., those not of current interest). This is particularly well suited for in situ analysis, as the core simulation may therefore be structured to make available a substantial array of internal data, knowing that only those parts that are needed at runtime will actually be exported. These properties contrast with the typical assumptions made by communication infrastructures like MPI, where the domain of executing processes is initialized at launch and cannot grow or shrink for the remainder of the execution.

With Flexpath, one can construct and dynamically manage or change the data processing pipelines or workflows needed for runtime analysis of the large volumes of this output data in ways that meet the following four design requirements of these sorts of applications: (1) decouple analytics services from simulation codes, (2) maintain levels of performance similar to those obtained by analytics routines statically embedded with simulations, (3) permit those pipelines to cross node and/or machine boundaries, and (4) support the creation of higher level methods for managing these pipelines. Sample management constructs built in our own previous work \cite{containers}, for example, have balanced pipeline operations to ensure QoS and have implemented transactional constructs with the goal of providing ACID properties for select online analytics~\cite{serviceaugmentation,clustertxn}.

\begin{figure*}[htbp]
\centering
\includegraphics[width=.5\textwidth]{figures/concept-a}
\caption{Brokered Publish Subscribe System}
\vspace{-.2in}
\label {f:pubsub-brokered}
\end{figure*}

\begin{figure*}[htbp]
\centering
\includegraphics[width=.5\textwidth]{figures/concept-b}
\caption{Flexpath: Non-brokered publish-subscribe based on a peer to peer model.}
\vspace{-.2in}
\label {f:flexpath}
\end{figure*}


Flexpath's pub/sub communication mechanism, key to meeting design objective (1), obtains flexibility for component-component communications, without the performance penalties incurred by traditional broker-based pub/sub infrastructures. This technical contribution is achieved by using direct connections between interacting components, including the scatter-gather or MxN communications needed across different communicating internally parallelized analytics components. This high performance implementation for such peer-to-peer techniques utilizes a subscription implementation, allowing readers to specify derived versions of messages, e.g., to receive only those slices of data objects they require, as well as registering dynamic transformations of typed objects when there are mismatches between publishers and subscribers, e.g. row to column order array conversions.

With regards to the need to maintain performance in cross-platform environments (design objectives 2 and 3), Flexpath has been built to leverage multiple underlying communi- cation protocols, ranging from a shared memory protocol employed for on-node communications, to the RDMA-based protocols existing on high end machines, to the TCP/IP protocols required for linking remote collaborators. As is described in Section IV, much of this comes from inheriting a multi-modal connection management system through the EVPath framework. Finally, with regards to design object (4)’s concerns for management, Flexpath’s approach allows for it to export monitoring data and management ’hooks’ with which higher level management methods can be realized. As will be seen later, we utilize some simple workflow-level management schemas in this work, but future work will extend the complexity and robustness of this feature of the system.

Figures \ref{f:pubsub-brokered} and \ref{f:flexpath} depict the conceptual design of the Flexpath system and contrasts it with a traditional publish/subscribe implementation that uses a brokered approach. Flexpath pushes the functionality typically found in the broker into the publishers and subscribers themselves and uses a peer-to-peer model for coordinating subscriptions, data eviction, event notification, etc across publishers and subscribers.

\section{SODA:Science-driven Orchestration of Data Analytics}

This section describes the {\em SODA} approach \cite{soda}
to managing dynamic I/O and analytics pipelines on high end machines (Fig.~\ref{F:container-workflow}). SODA permits developers to embed 
analytics tasks into a componentized, dynamically managed execution and messaging framework, called a {\em workstation.}
Such workstations have well defined inputs 
and outputs~\cite{adios}, can be parallel (MPI or threads), and may exhibit inter-workstation dependencies. 
Entire I/O pipelines can be constructed by chaining workstations along their I/O paths. 

\begin{figure}[!htb] 
\centering \includegraphics[width=0.8\columnwidth]{figures/soda-model} 
%\vspace{0.3cm} 
\caption{High-level view of the SODA framework.} 
\label{F:container-workflow} 
\end{figure}

SODA offers controlled resource usage, per-component orchestration, and
metric-driven operation. Controlled resource usage means workstations provide
and manage resources for the component mapped to it. Per-component
orchestration means that a workstation can offer customized orchestration
operations ensuring a component's local properties are not
violated. Finally, metric-driven operation means that workstations
are continually monitored to provide the runtime with the necessary information
needed to enforce user or application specific metrics.

SODA also provides fault-resilient management through transactional techniques
that guarantee control and orchestration actions taken by SODA do not place
components into inconsistent
states~\cite{clustertxn}. For example, SODA can prevent resource use until
a different workstation has fully relinquished the resource.  Such requirements
become important as I/O pipelines scale geographically~\cite{multi-physics} as
network partitions or data center outages can render parts of the pipeline
inoperable.

The workstation's I/O interfaces are similar in concept to those used in modern 
Service Oriented Architectures (SOA). The workstation is comprised of a set of \textit{active replicas} and a \textit{workstation orchestrator} overseeing its execution.

Unlike the replication techniques used in fault tolerant systems
where replicas have identical internal states\cite{bridges-replication}, active replicas in workstations are key to obtaining scalability:
with traditional replication, each replica performs redundant computations on the same data items whereas active replicas 
perform their computations on different epochs of data assigned to them. In the current implementation, 
data is assigned to active replicas in a round-robin fashion, but additional communication patterns can be supported.  Using active
replicas, a workstation orchestrator can increase its degree of parallelism by
spawning a new replica. While this is similar to how Map-Reduce jobs scale,
note that an individual data epoch may only be able to be processed by a fixed
process count and that scalability comes from overlapping processing of
different epochs.

The workstation orchestrator provides several functions. First, it collects and organizes relevant
monitoring data from its active replicas and delivers this information to a higher-level
orchestrator. Second, it provides metadata services for its replicas and contains end-point information for replicas
in neighboring workstations. Third, it contains potentially custom management primitive implementations
which allow them to respond to management requests from higher-level orchestrators.

In \cite{soda}, we evaluated this system with two real science applications and workflows, LAMMPS \cite{lammps}, along with
the SmartPointer \cite{smartp} analysis workflow, and GTS \cite{gts}, with an FFT analysis code. With these two use-cases, we
constructed performance-based policies to implement elasticity to recover from a workflow bottleneck, a data-aware policy to
steer the workflow based on features detected in the data, and a set of resilience policies to preserve the workflow when
components fail. For more details on the experiments and the results, please see \cite{soda}.

\section{Proposed Implementation: CoApp Runtime}

In the current implementation, Flexpath and SODA only consider the in situ mode where analytics are executing on separate ``staging'' nodes, and
thus, node consolidation policies are not yet considered. Additionally, SODA's current capabilities do not have abstractions that allow it to
introspect and interact with lower-level runtimes such as user-space CPU schedulers \cite{goldrush} or modified CUDA runtimes that allow for GPU
multi-tenancy \cite{landrush}.

\begin{figure}[!htb] 
\centering \includegraphics[width=0.98\columnwidth]{figures/coapps} 
\caption{High-level view of the CoApps middleware with two example policies} 
\label{f:coapps} 
\end{figure}

Along these fronts, my thesis will propose the Co-Apps runtime for in situ analysis workflows. In particular, the Co-App runtime will
will (1) extend the Flexpath and SODA systems to allow for node-sharing in situ use cases; (2) will extend the SODA system to create abstractions
to introspect and control lower-level software runtimes so they can respond to direction from higher-level managers or runtimes; (3) will explore
policies that can take into consideration node heterogeneity and placement options; and (4) evaluate the Co-Apps runtime using real science applications
and analysis workflows.

Figure \ref{f:coapps} depitcs the CoApp runtime design. With this model, the workstation abstraction is extended to not only span nodes, but to also
share nodes with other workstations. By enabling this functionality, the system can take advantage of node consolidation to reduce data movement and
also increase node utilization and lowering the number of aggregate resources used. By enabling more placement capabilities, we hope to be able to
provide stronger guarantees in regards to meeting end-user SLAs.

An important aspect here is the method in which the CoApp runtime interacts with the lower-level runtimes to try to meet a user specified SLA. Here,
we envision a ``grey-box'' model where higher-level managers can query lower-level runtimes for some performance information and then
delegate portions of an SLA and control to the lower level runtimes. By using this approach,
we hope to achieve greater scalability as the CoApp runtime will not have to micromanage lower-level runtimes. Instead, the Co-App runtime can
periodically introspect into the runtime to determine if sufficient progress is being made. In the case that progress isn't being made, perhaps
because of a bad management decision, the CoApps runtime will need to have ways of either rolling-back management operations or creating an entirely
new mapping of the in situ components to the resources. 


\section{Timeline of Work}

\begin{itemize}
\item Explore runtimes to grey box (i.e., LandRush, GoldRush, Flexpath, MPI) (May - June)
\item Implement CoApps runtime (May - June)
\item Evaluation (June - July)
\item Thesis writing (June - August)
\item Defense (August)
\end{itemize}
