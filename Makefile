# LaTeX Makefile for use with git and command file 
FILE=thesis

all: touch $(FILE).pdf clean_intermediate

$(FILE).pdf: $(FILE).tex 
	pdflatex $(FILE) 
	bibtex $(FILE)
	pdflatex $(FILE)
	pdflatex $(FILE)

.PHONY: clean clean_intermediate

clean_intermediate:
	rm -f *.aux *.blg *.out *.bbl *.log *.toc

clean: clean_intermediate
	rm -f $(FILE).pdf

touch:
	touch thesis.tex
